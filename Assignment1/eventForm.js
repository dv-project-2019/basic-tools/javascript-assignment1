//protect the problem about code
"use strict";

function addNewForm(){
    var col = document.createElement('div');
    col.setAttribute('class', 'col');

    var node = document.createElement('p');
    node.innerHTML = 'Please fill in this form to create account';
    node.setAttribute('class','lead');

    var node2 = document.createElement('div');
    node2.setAttribute('class', 'form-group row');

    var node3 = document.createElement('div');
    node3.setAttribute('class', 'col');
    var node4 = document.createElement('input');
    node4.setAttribute('type', 'text');
    node4.setAttribute('class', 'form-control');
    node4.setAttribute('placeholder', 'First name');
    node3.appendChild(node4);
    node2.appendChild(node3);

    var node5 = document.createElement('div');
    node5.setAttribute('class', 'col');
    var node6 = document.createElement('input');
    node6.setAttribute('type', 'text');
    node6.setAttribute('class', 'form-control');
    node6.setAttribute('placeholder', 'Last name');
    node5.appendChild(node6);
    node2.appendChild(node5);

    var node7 = document.createElement('div');
    node7.setAttribute('class', 'row');
    var node8 =  document.createElement('div');
    node8.setAttribute('class', 'col');
    var node9 =  document.createElement('div');
    node9.setAttribute('class', 'form-group');
    var node10 =  document.createElement('input');
    node10.setAttribute('type', 'email');
    node10.setAttribute('class', 'form-control');
    node10.setAttribute('placeholder', 'Email');
    node9.appendChild(node10);
    node8.appendChild(node9);
    node7.appendChild(node8);

    col.appendChild(node);
    col.appendChild(node2);
    col.appendChild(node7);

    var placeHolder = document.getElementById('placeholder');
    placeHolder.appendChild(col);

}