//protect the problem about code
"use strict";

function createTable(){
    var table = document.createElement('table');
    table.setAttribute('class', 'table table-striped');

    var thead = document.createElement('thead');
    var tr1 = document.createElement('tr');
    var th1 = document.createElement('th');
    th1.setAttribute('scope', 'col');
    th1.innerText = '#';
    var th2 = document.createElement('th');
    th2.setAttribute('scope', 'col');
    th2.innerText = 'First';
    var th3 = document.createElement('th');
    th3.setAttribute('scope', 'col');
    th3.innerText = 'Last';
    var th4 = document.createElement('th');
    th4.setAttribute('scope', 'col');
    th4.innerText = 'handel';

    tr1.appendChild(th1);
    tr1.appendChild(th2);
    tr1.appendChild(th3);
    tr1.appendChild(th4);
    thead.appendChild(tr1);
    table.appendChild(thead);

    var tbody = document.createElement('tbody');
    var tr2 = document.createElement('tr');
    var th5 = document.createElement('th');
    th5.setAttribute('scope', 'row');
    th5.innerText = '1';
    var td22 = document.createElement('td');
    td22.innerText = 'Mark';
    var td23 = document.createElement('td');
    td23.innerText = 'Otto';
    var td24 = document.createElement('td');
    td24.innerText = '@mdo';
    tr2.appendChild(th5);
    tr2.appendChild(td22);
    tr2.appendChild(td23);
    tr2.appendChild(td24);
    tbody.appendChild(tr2);

    var tr3 = document.createElement('tr');
    var th6 = document.createElement('th');
    th6.setAttribute('scope', 'row');
    th6.innerText = '2';
    var td32 = document.createElement('td');
    td32.innerText = 'Jacob';
    var td33 = document.createElement('td');
    td33.innerText = 'Thornton';
    var td34 = document.createElement('td');
    td34.innerText = '@fat';
    tr3.appendChild(th6);
    tr3.appendChild(td32);
    tr3.appendChild(td33);
    tr3.appendChild(td34);
    tbody.appendChild(tr3);

    var tr4 = document.createElement('tr');
    var th7 = document.createElement('th');
    th7.setAttribute('scope', 'row');
    th7.innerText = '3';
    var td42 = document.createElement('td');
    td42.innerText = 'Larry';
    var td43 = document.createElement('td');
    td43.innerText = 'The bird';
    var td44 = document.createElement('td');
    td44.innerText = '@twitter';
    tr4.appendChild(th7);
    tr4.appendChild(td42);
    tr4.appendChild(td43);
    tr4.appendChild(td44);
    tbody.appendChild(tr4);
    table.appendChild(tbody);

    var topic = document.createElement('h3');
    topic.innerHTML = "Create from js"

    var show = document.getElementById('placeholder');
    show.appendChild(topic);
    show.appendChild(table);
}