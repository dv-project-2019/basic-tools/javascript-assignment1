//protect the problem about code
"use strict";

function submitNumber() {
    var number = document.getElementById('inputNumber').value;
    console.log('input number: ' + number);
    if (number > 0) {
        generateTable(number);
    } else if (number <= 0) {
        alert('Please input number more than 0');
    }
}

function generateTable(number) {
    console.log('Generate table');
    var table = document.createElement('table');
    table.setAttribute('class', 'table');

    var thead = document.createElement('thead');
    var tbody = document.createElement('tbody');

    for (var i = 0; i <= number; i++) {
        if (i == 0) {
            var tri = document.createElement('tr');
            var thi = document.createElement('th');
            thi.setAttribute('scope', 'col');
            thi.innerText = '#';
            tri.appendChild(thi);
            thead.appendChild(tri);
            console.log(tri);
        } else {
            var tri = document.createElement('tr');
            var thi = document.createElement('th');
            thi.setAttribute('scope', 'row');
            thi.innerText = i;
            tri.appendChild(thi);
            tbody.appendChild(tri);
            console.log(tri);
        }
    }

    table.appendChild(thead);
    table.appendChild(tbody);
    console.log(table);

    var show = document.getElementById('placeholder');
    show.appendChild(table);
}